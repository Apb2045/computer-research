/* Use ASMLibrary(Active Shape Model Library) -- A compact SDK for face alignment
from Yao Wei <njustyw@gmail.com> (http://visionopen.com/members/yaowei/) to extract 
Facial features and same the features to files for classification.
 
 Author: YingLi Tian
 Date: June 2010

Do face tracking by using active shape model 

   Usage: fit -m model_file -h cascade_file {-i image_file | -v video_file | -c | -b} -n n_iteration
	 -i   image_file
     -v   video_file
	 -c	  live camera
	 -b   batch video file from a text file contains a list of video files

   Examples:
   # Image alignment on an image using 30 iterations
   > fit -m my.amf -h haarcascade_frontalface_alt2.xml -i aa.jpg -n 30

   # Face tracking on a video file
   > fit -m my.amf -h haarcascade_frontalface_alt2.xml -v bb.avi -n 25

   # Face tracking on live camara
   > fit -m my.amf -h haarcascade_frontalface_alt2.xml -c 
   
   # Face tracking on a batch file of videos
   > fit -m my.amf -h haarcascade_frontalface_alt2.xml -v aa.bv -n 25

   */

#include <vector>
#include <string>
#include <iostream>

#include "ASMModelFit.h"
#include "Mouth.h"

using namespace std;


static void usage_fit()
{
	printf("Usage: fit -m model_file -h cascade_file "
		"{-i image_file | -v video_file | -c | -b batch_video_file} -n n_iteration\n\n\n");
	exit(0);
}


int main(int argc, char *argv[])
{
	asmfitting fit_asm;
	char* model_name = NULL;
	char* cascade_name = NULL;
	char* filename = NULL;
	int use_camera = 0;
	int image_or_video = -1;
	int i;
	int n_iteration = 20;


	if(1 == argc)	usage_fit();
	for(i = 1; i < argc; i++)
	{
		if(argv[i][0] != '-') usage_fit();
		if(++i > argc)	usage_fit();
		switch(argv[i-1][1])
		{
		case 'm':
			model_name = argv[i];
			break;
		case 'h':
			cascade_name = argv[i];
			break;
		case 'i':
			if(image_or_video >= 0 || use_camera)
			{
				fprintf(stderr, "only process image/video/camera once\n");
				usage_fit();
			}
			filename = argv[i];
			image_or_video = 'i';
			break;
		case 'v':
			if(image_or_video >= 0 || use_camera)
			{
				fprintf(stderr, "only process image/video/camera once\n");
				usage_fit();
			}
			filename = argv[i];
			image_or_video = 'v';
			break;
		case 'b':
			if(image_or_video >= 0 || use_camera)
			{
				fprintf(stderr, "only process image/video/camera once\n");
				usage_fit();
			}
			filename = argv[i];
			image_or_video = 'b';
			break;
		case 'c':
			if(image_or_video >= 0)
			{
				fprintf(stderr, "only process image/video/camera once\n");
				usage_fit();
			}
			use_camera = 1;
			break;
		case 'H':
			usage_fit();
			break;
		case 'n':
			n_iteration = atoi(argv[i]);
			break;
		default:
			fprintf(stderr, "unknown options\n");
			usage_fit();
		}
	}

	if(fit_asm.Read(model_name) == false)
		return -1;
	
	if(init_detect_cascade(cascade_name) == false)
		return -1;
	
	// case 1: process video, here we assume that the video contains only one face,
	// if not, we process with the most central face
	if(image_or_video == 'v')
	{
		int frame_count;
		asm_shape shape, detshape;
		bool flag = false;
		IplImage* image; 
		/* NOTE: the image must not be released, it will be dellocated automatically
		by the class asm_cam_or_avi*/
		int j, k;
		int iNump;
		CvPoint2D32f pt;
	
		frame_count = open_video(filename);
		if(frame_count == -1)	return false;

		cvNamedWindow("ASM-Search",1);

		for(j = 0; j < frame_count; j ++)
		{
			double t = (double)cvGetTickCount();
			printf("Tracking frame %04i: ", j);
			
			image = read_from_video(j);
			
			if(j == 0 || flag == false)
			{
				//Firstly, we detect face by using Viola_jones haarlike-detector
				flag = detect_one_face(detshape, image);
				
				//Secondly, we initialize shape from the detected box
				if(flag) 
				{	
					InitShapeFromDetBox(shape, detshape, fit_asm.GetMappingDetShape(), fit_asm.GetMeanFaceWidth());
				}
				else goto show;
			}
			
			//Thirdly, we do image alignment 
			flag = fit_asm.ASMSeqSearch(shape, image, j, true, n_iteration);
			
			//If success, we draw and show its result
			if(flag) 
			{
				fit_asm.Draw(image, shape);

				//understand landmarks of face shape model
				iNump =	shape.NPoints();
				if (iNump > 0)
				{
					for(k = 0; k < iNump; k ++)
					{
						pt = shape[k];
						int ptx = (int) pt.x;
						int pty = (int) pt.y;
						if(k>=67 && k<68)
							DrawCircleFeature(image, ptx, pty, 5, 1);
					}
				}

			}
show:
			cvShowImage("ASM-Search", image);
			cvWaitKey(1);
			
			t = ((double)cvGetTickCount() -  t )/  (cvGetTickFrequency()*1000.);
			printf("ASM fitting time cost: %.2f millisec\n", t);
		}

		close_video();
	}

	// case 2: process batch file of videos // need more coding here
	else if(image_or_video == 'b')
	{
		int frame_count;
		asm_shape shape, detshape;
		bool flag = false;
		IplImage* image; 
		/* NOTE: the image must not be released, it will be dellocated automatically
		by the class asm_cam_or_avi*/
		int j;
	
		frame_count = open_video(filename);
		if(frame_count == -1)	return false;

		printf("Opening video file: %25s", filename);

		cvNamedWindow("ASM-Search",1);

		for(j = 0; j < frame_count; j ++)
		{
			double t = (double)cvGetTickCount();
			printf("Tracking frame %04i: ", j);
			
			image = read_from_video(j);
			
			if(j == 0 || flag == false)
			{
				//Firstly, we detect face by using Viola_jones haarlike-detector
				flag = detect_one_face(detshape, image);
				
				//Secondly, we initialize shape from the detected box
				if(flag) 
				{	
					InitShapeFromDetBox(shape, detshape, fit_asm.GetMappingDetShape(), fit_asm.GetMeanFaceWidth());
				}
				else goto showb;
			}
			
			//Thirdly, we do image alignment 
			flag = fit_asm.ASMSeqSearch(shape, image, j, true, n_iteration);
			
			//If success, we draw and show its result
			if(flag) fit_asm.Draw(image, shape);
showb:
			cvShowImage("ASM-Search", image);
			cvWaitKey(1);
			
			t = ((double)cvGetTickCount() -  t )/  (cvGetTickFrequency()*1000.);
			printf("ASM fitting time cost: %.2f millisec\n", t);
		}

		close_video();
	}
	// case 2: process image, we can process multi-person image alignment
	// also you can process single face alignment by coding like this
	// asm_shape detshape, shape;	
	// bool flag = face_detect.DetectCentralFace(detshape, image);
	// if(flag) asm_common::InitShapeFromDetBox(shape, detshape, 
	//		fit_asm.GetMappingDetShape(), fit_asm.GetMeanFaceWidth());
	// fit_asm.Fitting(shape, image, n_iteration);
	// shape.Write(stdout); //print result
	// for(int l = 0; l < shape.NPoints(); l++)
	//		printf("(%g, %g) ", shape[i].x, shape[i].y);
	else if(image_or_video == 'i')
	{
		IplImage * image = cvLoadImage(filename, 1);
		if(image == 0)
		{
			fprintf(stderr, "Can not Open image %s\n", filename);
			exit(0);
		}

		double t = (double)cvGetTickCount();
		int nFaces;
		asm_shape *shapes = NULL, *detshapes = NULL;
		
		// step 1: detect face
		bool flag =detect_all_faces(&detshapes, nFaces, image);
		
		// step 2: initialize shape from detect box
		if(flag)
		{
			shapes = new asm_shape[nFaces];
			for(int i = 0; i < nFaces; i++)
			{
				InitShapeFromDetBox(shapes[i], detshapes[i], fit_asm.GetMappingDetShape(), fit_asm.GetMeanFaceWidth());
			}
		}
		else 
		{
			fprintf(stderr, "This image doesnot contain any faces!\n");
			exit(0);
		}
		
		// step 3: image alignment fitting
		fit_asm.Fitting2(shapes, nFaces, image, n_iteration);
		
		t = ((double)cvGetTickCount() -  t )/  (cvGetTickFrequency()*1000.);
			printf("ASM fitting time cost: %.2f millisec\n", t);
			
		// step 4: draw and show result in GUI
		for(int i = 0; i < nFaces; i++)
		{
			fit_asm.Draw(image, shapes[i]);
		}
		
		cvSaveImage("result.jpg", image);
		cvNamedWindow("Fitting", 1);
		cvShowImage("Fitting", image);	
		cvWaitKey(0);			
		cvReleaseImage(&image);

		// step 5: free resource
		delete[] shapes;
		free_shape_memeory(&detshapes);
	}
	// case 3: process camera
	else if(use_camera)
	{
		asm_shape shape, detshape;
		bool flag = false;
		IplImage* image; 
		int j = 0;
		
		if(open_camera(0) == false)
			return -1;

		cvNamedWindow("ASM-Search",1);

		while(1)
		{
			// NOTE: when the parameter is set 1, we can read from camera
			image = read_from_camera();
			
			if(flag == false)
			{
				//Firstly, we detect face by using Viola_jones haarlike-detector
				flag = detect_one_face(detshape, image);
				
				//Secondly, we initialize shape from the detected box
				if(flag)
				{
					InitShapeFromDetBox(shape, detshape, fit_asm.GetMappingDetShape(), fit_asm.GetMeanFaceWidth());
					j ++;
				}
				else 
					goto show2;
			}
			
			//Thirdly, we do image alignment 
			flag = fit_asm.ASMSeqSearch(shape, image, j, true, n_iteration);
			
			//If success, we draw and show its result
			if(flag) fit_asm.Draw(image, shape);
show2:
			cvShowImage("ASM-Search", image);
			cvWaitKey(1);
		}

		close_camera();
	}

    return 0;
}



