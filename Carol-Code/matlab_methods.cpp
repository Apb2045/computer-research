#include <stdio.h>

#include "engine.h"
#include "matrix.h"


static Engine *ep;

static void put_variable(const char * varname, mxArray * array) {

	engPutVariable(ep, varname, array);
}

mxArray * create_numeric_array(mwSize size, mxClassID classid) {

	return mxCreateNumericMatrix(1, size, classid, mxREAL); // 0 = not complex
}

void openMatlab() {

	if (!(ep = engOpen("\0"))) {
		fprintf(stderr, "\nCan't start MATLAB engine\n");	
		return;
	}
}

void sendToMatlab(float mouthdata[19]){

	mxArray* mouth = create_numeric_array(19 , mxDOUBLE_CLASS);
	//;

	put_variable("mouth",mouth);

}


void closeMatlab() {

	engEvalString(ep, "close;");
	engClose(ep);
}
