/*********************************************************************
 *File: Svmlib_c.cpp
 *Purpose: wrapper class for svmlib
 *
 *Author: Shizhi Chen
 *Email: schen21@ccny.cuny.edu
 *Date: 4/19/2012
 *********************************************************************/

#include "Svmlib_c.h"

Svmlib_c::Svmlib_c(bool scale_flag,string model_file, string xml_file,float scale_min, float scale_max, int nFold)
{
	scaleFlag = scale_flag;
	model = NULL;
	modelFile = model_file;
	xmlFile = xml_file;
	scaleMin = scale_min;
	scaleMax = scale_max;
	numFoldCV = nFold;
}

Svmlib_c::Svmlib_c()
{
	scaleFlag = true;
	model = NULL;
	modelFile = "svm.model";
	xmlFile = "train.xml";
	scaleMin = 0.0f;
	scaleMax = 1.0f;
	numFoldCV = 5;
	
}

Svmlib_c::~Svmlib_c()
{
	svm_free_and_destroy_model(&model);
	
}

//scale dataM along each dimension between the feat_mins and feat_maxs at the corresponding feature dimension.
//assume trainDataM has the type of float (CV_32FC1). each row corresponds to one sample feature.
cv::Mat Svmlib_c::scaleData(cv::Mat dataM, int featDim, float *feat_mins, float *feat_maxs, float scale_min, float scale_max)
{
	if (dataM.cols != featDim)
	{
		printf("Error: featDim is not same as the column size of dataM.\n");
		exit(-1);
	}

	int nSample = dataM.rows;

	cv::Mat scaledDataM(nSample,featDim,CV_32FC1);

	for (int i=0;i<nSample;i++)
	{
		float *dataM_row = dataM.ptr<float>(i);
		float *scaledDataM_row = scaledDataM.ptr<float>(i);

		for (int j=0;j<featDim;j++)
		{
			if (feat_mins[j]==feat_maxs[j])
				scaledDataM_row[j] = 0.0f;
			else
				scaledDataM_row[j] = (dataM_row[j]-feat_mins[j])/(feat_maxs[j]-feat_mins[j])*(scale_max-scale_min)+scale_min;
		}
	}

	return scaledDataM;
}

//find min and max along each dimension from the trainDataM; assume trainDataM has the type of float (CV_32FC1).
//each row corresponds to one sample feature. feat_mins/feat_maxs is a row vector with type of CV_32FC1
void Svmlib_c::findFeatMinMax(cv::Mat trainDataM, cv::Mat &feat_mins, cv::Mat &feat_maxs)
{
	int nDim = trainDataM.cols;
	feat_mins.create(1,nDim,CV_32FC1);
	feat_maxs.create(1,nDim,CV_32FC1);

	float *feat_mins_row = feat_mins.ptr<float>(0);
	float *feat_maxs_row = feat_maxs.ptr<float>(0);

	findFeatMinMax(trainDataM, nDim, feat_mins_row, feat_maxs_row);
}

//find min and max along each dimension from the trainDataM; assume trainDataM has the type of float (CV_32FC1).
//each row corresponds to one sample feature.
void Svmlib_c::findFeatMinMax(cv::Mat trainDataM, int featDim, float *feat_mins, float *feat_maxs)
{
	if (trainDataM.cols != featDim)
	{
		printf("Error: featDim is not same as the column size of trainDataM.\n");
		exit(-1);
	}
	
	//initialize feat_mins and feat_maxs
	for (int j=0;j<featDim;j++)
	{
		feat_mins[j] = LARGE_POSNUMBER;
		feat_maxs[j] = LARGE_NEGNUMBER;
	}

	int nSample = trainDataM.rows;

	for (int i=0;i<nSample;i++)
	{
		float *trainDataM_row = trainDataM.ptr<float>(i);

		for (int j=0;j<featDim; j++)
		{
			if (trainDataM_row[j]<feat_mins[j])
				feat_mins[j] = trainDataM_row[j];

			if (trainDataM_row[j]>feat_maxs[j])
				feat_maxs[j] = trainDataM_row[j];
		}
	}
}

//convert training data in cv::Mat format to the svm_problem format. 
//each row of dataM correspond to one sample with float type. labelV is a column vector with float type.
//Note: need free prob.y prob.x and x_space after model generated from svm_train.
void Svmlib_c::convertMatToSvmProb(cv::Mat dataM, cv::Mat labelV, struct svm_problem &prob, struct svm_node ** x_space_ptr)
{
	int nSample = dataM.rows;
	int nFeatDim = dataM.cols;

	if (nSample != labelV.rows)
	{
		printf("Error: number of rows for labelV and dataM is not same.\n");
		exit(-1);
	}

	if (labelV.cols != 1)
		{
		printf("Error: labelV is not a column vector.\n");
		exit(-1);
	}

	prob.l = nSample;
	prob.y = (double *) malloc(nSample*sizeof(double));

	for (int i=0;i<nSample;i++)
	{
		prob.y[i] = (double) labelV.at<float>(i,0);
	}

	int nSvmNode = 0;

	for (int i=0;i<nSample;i++)
	{
		float *dataM_row = dataM.ptr<float>(i);

		for (int j=0;j<nFeatDim;j++)
		{
			if (dataM_row[j] != 0.0f)
				nSvmNode++;
		}
	}

	nSvmNode += nSample;

	struct svm_node* x_space = (struct svm_node*) malloc(nSvmNode*sizeof(struct svm_node));
	
	prob.x = (struct svm_node **) malloc(nSample*sizeof(struct svm_node*));

	int svmNodeIndx = 0;

	for (int i=0;i<nSample;i++)
	{
		float *dataM_row = dataM.ptr<float>(i);
		
		prob.x[i] = &x_space[svmNodeIndx];
		for (int j=0;j<nFeatDim;j++)
		{
			if (dataM_row[j] != 0.0f)
			{
				x_space[svmNodeIndx].index = j+1; //1-based index for svm node
				x_space[svmNodeIndx].value = (double) dataM_row[j];

				svmNodeIndx++;
			}	
		}

		x_space[svmNodeIndx].index = -1;
		svmNodeIndx++;
	}

	*x_space_ptr = x_space;

	//debug:
	//if (svmNodeIndx != nSvmNode)
	//{
	//	printf("Error: svmNodeIndx != nSvmNode.\n");
	//	exit(-1);
	//}
}

//set RBF kernel parameters
struct svm_parameter Svmlib_c::setParam_RBF(double gamma, double C)
{
	struct svm_parameter param;

	// default values
	param.degree = 3;
	param.coef0 = 0;
	param.nu = 0.5;
	param.eps = 1e-3;
	param.p = 0.1;
	param.shrinking = 1;
	param.probability = 0;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;

	param.svm_type = C_SVC;
	param.kernel_type = RBF;
	param.cache_size = 100; //cache memory size 100 MB

	param.gamma = gamma;
	param.C = C;

	return param;
}

//find best RBF parameters by using cross validation
void Svmlib_c::findBestParam_RBF(svm_problem &prob, double* gammas, double* Cs, int numParam, int nFold, double &best_gamma, double &best_C,
								 bool verbose)
{
	double *predict_labels = (double *) malloc(prob.l*sizeof(double));
	float *accuracys = (float *) malloc(numParam*sizeof(float));

	for (int i=0;i<numParam;i++)
	{
		int nCorrect = 0;

		struct svm_parameter param = setParam_RBF(gammas[i],Cs[i]);
		svm_cross_validation(&prob,&param,nFold,predict_labels);
		
		for (int j=0;j<prob.l;j++)
		{
			if (predict_labels[j] == prob.y[j])
				nCorrect++;
		}

		accuracys[i] = (float) nCorrect/((float) prob.l);
		
		svm_destroy_param(&param);
	}

	int best_indx = getMaxIndx(accuracys,numParam);

	best_gamma = gammas[best_indx];
	best_C = Cs[best_indx];

	if (verbose)
	{
		for (int i=0;i<numParam;i++)
		{
			printf("gamma = %0.2f, C = %0.2f: accuracy = %0.1f%%\n",gammas[i],Cs[i],100.0f*accuracys[i]);
		}

		printf("\nbest_gamma = %0.2f, best_c = %0.2f accuracy = %0.1f%%\n",gammas[best_indx],Cs[best_indx],100.0f*accuracys[best_indx]);
	}


	free(accuracys);
	free(predict_labels);
}

//train model, and calculate featMins and featMaxs if scaleFlag is true.
//Note: dataM has the type of float, with each row represent one sample's data; labelV is column vector with float type, log2gs, log2cs are row vector with float type
void Svmlib_c::train_RBF(cv::Mat dataM, cv::Mat labelV, cv::Mat log2gs, cv::Mat log2cs)
{
	cv::Mat gammas;
	cv::Mat Cs;

	getParamCandidates_RBF(log2gs, log2cs, gammas, Cs);

	int numParam = gammas.cols;
	
	if (Cs.cols != numParam)
	{
		printf("Error: gammas and Cs should have same coloumn size.\n");
		exit(-1);
	}
	
	int nSample = dataM.rows;
	if (labelV.rows != nSample)
	{
		printf("Error: dataM and labelV should have same rows.\n");
		exit(-1);
	}
	
	struct svm_problem prob; 
	struct svm_node *x_space;
	
	if (scaleFlag)
	{
		findFeatMinMax(dataM, featMins, featMaxs);
		int nFeatDim = featMins.cols;
		float *featMins_row = featMins.ptr<float>(0);
		float *featMaxs_row = featMaxs.ptr<float>(0);
		cv::Mat scaledDataM = scaleData(dataM,nFeatDim,featMins_row, featMaxs_row,scaleMin, scaleMax);
		
		convertMatToSvmProb(scaledDataM, labelV, prob, &x_space);
		
	}
	else 
	{
		convertMatToSvmProb(dataM, labelV, prob, &x_space);
	}
	
	double best_gamma, best_C;
	
	//show
	//printf("Finding best_gamma and best_C ......\n");

	if (numParam > 1)
	{	
		//convert cv::Mat to double pointer
		float *gammas_row = (float *) gammas.ptr<float>(0);
		float *Cs_row = (float *) Cs.ptr<float>(0);
		double *gammas_dPtr = (double *) malloc(numParam*sizeof(double));
		double *Cs_dPtr = (double *) malloc(numParam*sizeof(double));
		
		for (int i=0;i<numParam;i++)
		{
			gammas_dPtr[i] = (double) gammas_row[i];
			Cs_dPtr[i] = (double) Cs_row[i];
		}
		
		findBestParam_RBF(prob, gammas_dPtr, Cs_dPtr, numParam, numFoldCV, best_gamma, best_C);
		
		free(gammas_dPtr);
		free(Cs_dPtr);
	}
	else //numParam ==1
	{
		best_C = (double) Cs.at<float>(0,0);
		best_gamma = (double) gammas.at<float>(0,0);
	}
	
	struct svm_parameter param = setParam_RBF(best_gamma, best_C);

	model = svm_train(&prob,&param);

	//save before releasing x_space since model->SV contains pointers to x_space.
	saveTrainModel();

	svm_free_and_destroy_model(&model);
	
	free(prob.y);
	free(prob.x);
	free(x_space);

	//debug
	//for (int j=0;j<model->l;j++)
	//{	
	//	int k=0;
	//	while (model->SV[j][k].index != -1)
	//	{
	//		printf("(%i, %0.2f)  ",model->SV[j][k].index,model->SV[j][k].value);
	//		k++;
	//	}

	//	printf("\n");
	//}

	
	svm_destroy_param(&param);
}

//save trained svm model from modelFile, and save featMins, featMaxs from xmlFile if scaleFlag is true
void Svmlib_c::saveTrainModel()
{
	if (scaleFlag)
	{
		cv::FileStorage fs(xmlFile,cv::FileStorage::WRITE);
		
		fs << "featMaxs" << featMaxs;
		fs << "featMins" << featMins;
		
		fs.release();
	}
	
	if (model == NULL)
	{
		printf("Error: model is NULL.\n");
		exit(-1);
	}
	
	if(svm_save_model(modelFile.c_str(),model))
	{
		printf("can't save model to file %s\n", modelFile.c_str());
		exit(-1);
	}
}

//load trained svm model from modelFile, and load featMins, featMaxs from xmlFile if scaleFlag is true.
//overide existing model, featMins and featMaxs if any.
void Svmlib_c::loadTrainModel()
{
	//if model exits
	if (model != NULL)
	{
		svm_free_and_destroy_model(&model);
	}
	
	model=svm_load_model(modelFile.c_str());
	
	if(model == NULL)
	{
		printf("Error: cannot open model file %s.\n",modelFile.c_str());
		exit(-1);
	}
	
	if (scaleFlag)
	{
		cv::FileStorage fs(xmlFile,cv::FileStorage::READ);
		
		fs["featMins"] >> featMins;
		fs["featMaxs"] >> featMaxs;
		
		fs.release();
	}
	
}

//predict labels of testing dataM. if scaleFlag is true, the testing dataM will be scaled.
//Note: dataM has the type of float, with each row represent one sample's data; 
//predictLabels has the dimension of nSample X 1 with float type (CV_32FC1).
cv::Mat Svmlib_c::predict(cv::Mat dataM)
{
	//load model
	loadTrainModel();

	int nSample = dataM.rows;
	
	cv::Mat dummy_labelV = cv::Mat::zeros(nSample,1,CV_32FC1);
	
	struct svm_problem prob; 
	struct svm_node *x_space;
	
	if (scaleFlag)
	{
		int nFeatDim = featMins.cols;
		
		if (dataM.cols != nFeatDim)
		{
			printf("Error: featMins has different feature dimension comparing to the input testing dataM.\n");
			exit(-1);
		}
		
		float *featMins_row = featMins.ptr<float>(0);
		float *featMaxs_row = featMaxs.ptr<float>(0);
		cv::Mat scaledDataM = scaleData(dataM,nFeatDim,featMins_row, featMaxs_row,scaleMin, scaleMax);
		
		convertMatToSvmProb(scaledDataM, dummy_labelV, prob, &x_space);
		
	}
	else 
	{
		convertMatToSvmProb(dataM, dummy_labelV, prob, &x_space);
	}

	cv::Mat predictLabels(nSample,1,CV_32FC1);
	
	for (int i=0; i< nSample; i++)
	{
		predictLabels.at<float>(i,0) = (float) svm_predict(model,prob.x[i]);
		 
	}
	
	free(prob.y);
	free(prob.x);
	free(x_space);
	
	return predictLabels;
}

//log2gs (1 X M) with float type, log2cs (1 X N) with float type are row vectors, they can have different sizes. 
//gammas and Cs are also row vectors with the same size (M*N) and float type, where each pair of gamma and C 
//in gammas and Cs represent the all possible combinations of 2^(log2g) and 2^(log2c).
void Svmlib_c::getParamCandidates_RBF(cv::Mat log2gs, cv::Mat log2cs, cv::Mat &gammas, cv::Mat &Cs)
{
	if (log2gs.type() != CV_32FC1 || log2cs.type() != CV_32FC1)
	{
		printf("Error: only CV_32FC1 is surpported for log2gs and log2cs.\n");
		exit(-1);
	}

	int M = log2gs.cols;
	int N = log2cs.cols;

	gammas.create(1,M*N,CV_32FC1);
	Cs.create(1,M*N,CV_32FC1);
	
	float *log2gs_row = log2gs.ptr<float>(0);
	float *log2cs_row = log2cs.ptr<float>(0);

	float *gammas_row = gammas.ptr<float>(0);
	float *Cs_row = Cs.ptr<float>(0);

	for (int i=0;i<M;i++)
	{
		for (int j=0;j<N;j++)
		{
			int idx = i*N+j;
			gammas_row[idx] = pow(2.0f,log2gs_row[i]);
			Cs_row[idx] = pow(2.0f,log2cs_row[j]);
		}
	}
}









