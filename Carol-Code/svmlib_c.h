/*********************************************************************
 *File: Svmlib_c.h
 *Purpose: wrapper class for svmlib
 *
 *Author: Shizhi Chen
 *Email: schen21@ccny.cuny.edu
 *Date: 4/19/2012
 *********************************************************************/

#ifndef Svmlib_c_H___
#define Svmlib_c_H___ 

#include "Utility.h"
//#include ".\from_others\svm\svm.h"
#include "svm.h"

class Svmlib_c
{
public:
	cv::Mat featMaxs;
	cv::Mat featMins;
	struct svm_model* model;
	
	bool scaleFlag;
	string modelFile; //store svm model
	string xmlFile;  //store featMaxs and featMins
	float scaleMin;
	float scaleMax;
	int numFoldCV; //number of folds for cross validation
	
	Svmlib_c();
	Svmlib_c(bool scale_flag,string model_file, string xml_file, float scale_min, float scale_max, int nFold);
	~Svmlib_c();
	
	//train model, and calculate featMins and featMaxs if scaleFlag is true.
	//Note: dataM has the type of float, with each row represent one sample's data; labelV is column vector with float type, log2gs, log2cs are row vector with float type
	void train_RBF(cv::Mat dataM, cv::Mat labelV, cv::Mat log2gs, cv::Mat log2cs);

	//predict labels of testing dataM. if scaleFlag is true, the testing dataM will be scaled.
	//Note: dataM has the type of float, with each row represent one sample's data; 
	//the returned predictLabels has the dimension of nSample X 1 with float type (CV_32FC1).
	cv::Mat predict(cv::Mat dataM);
	
	


private:
	
	//find min and max along each dimension from the trainDataM; assume trainDataM has the type of float (CV_32FC1).
	//each row corresponds to one sample feature. feat_mins/feat_maxs is a row vector with type of CV_32FC1
	void findFeatMinMax(cv::Mat trainDataM, cv::Mat &feat_mins, cv::Mat &feat_maxs);
	
	//find min and max along each dimension from the trainDataM; assume trainDataM has the type of float (CV_32FC1).
	//each row corresponds to one sample feature.
	void findFeatMinMax(cv::Mat trainDataM, int featDim, float *feat_mins, float *feat_maxs);
	
	//scale dataM along each dimension between the feat_mins and feat_maxs at the corresponding feature dimension.
	//assume dataM has the type of float (CV_32FC1). each row corresponds to one sample feature.
	cv::Mat scaleData(cv::Mat dataM, int featDim, float *feat_mins, float *feat_maxs, float scale_min, float scale_max);
	
	//convert training data in cv::Mat format to the svm_problem format. 
	//each row of dataM correspond to one sample with float type. labelV is a column vector with float type.
	//Note: need free prob.y prob.x and x_space after model generated from svm_train.
	void convertMatToSvmProb(cv::Mat dataM, cv::Mat labelV, struct svm_problem &prob, struct svm_node ** x_space_ptr);
	
	//set RBF kernel parameters
	struct svm_parameter setParam_RBF(double gamma, double C);
	
	//find best RBF parameters by using cross validation
	void findBestParam_RBF(svm_problem &prob, double* gammas, double* Cs, int numParam, int nFold, double &best_gamma, double &best_C,
						   bool verbose=true);

	//save trained svm model from modelFile, and save featMins, featMaxs from xmlFile if scaleFlag is true
	void saveTrainModel();

	//log2gs (1 X M) with float type, log2cs (1 X N) with float type are row vectors, they can have different sizes. 
	//gammas and Cs are also row vectors with the same size (M*N) and float type, where each pair of gamma and C 
	//in gammas and Cs represent the all possible combinations of 2^(log2g) and 2^(log2c).
	void getParamCandidates_RBF(cv::Mat log2gs, cv::Mat log2cs, cv::Mat &gammas, cv::Mat &Cs);

	//load trained svm model from modelFile, and load featMins, featMaxs from xmlFile if scaleFlag is true.
	//overide existing model, featMins and featMaxs if any.
	void loadTrainModel();

};

#endif
