/*********************************************************************
 *File: Utility.cpp
 *Purpose: all utility functions
 *
 *Author: Shizhi Chen
 *Email: schen21@ccny.cuny.edu
 *Date: 4/4/2012
 *********************************************************************/

#include "Utility.h"

//grap all files from a folder specified by "dir", which matched regular expression of "matchRegExpStr"
//then stored all files in the folders to a vector<string> "files"
//Note: need '\\' in c++ to represent '\' in perl regular expression, e.g. "\\.jpg$", 
/*
bool grapFiles (string dir,string matchRegExpStr, vector<string> &files)
{
	bool successFlag = false;
	DIR *dp;
	struct dirent *dirp;
	if((dp = opendir(dir.c_str())) == NULL) {
		printf("Warning: cannot open dir (%s).\n",dir.c_str());
		return successFlag;
	}
	
	regex rgx(matchRegExpStr);
	
	while ((dirp = readdir(dp)) != NULL) 
	{
		string fileName = dirp->d_name;
		if (regex_search(fileName,rgx))
			files.push_back(fileName);
		
	}
	closedir(dp);
	
	successFlag=true;
	return successFlag;
}

//check if file in dir director exist.

bool isFileExist(string dir,string file)
{
	bool successFlag = false;
	
	DIR *dp;
	struct dirent *dirp;
	if((dp = opendir(dir.c_str())) == NULL) 
	{
		printf("Warning: cannot open dir (%s).\n",dir.c_str());
		return successFlag;
	}

	while ((dirp = readdir(dp)) != NULL) 
	{
		string fileName = dirp->d_name;

		if (strcmp(fileName.c_str(),file.c_str())==0) //found queried file
		{
			successFlag = true;
			break;
		}
	}

	closedir(dp);

	return successFlag;
}
*/
//trim the leading and trailing space of the input string.
void trimString(string& str)
{
	regex rgx1("^\\s*");
	regex rgx2("\\s*$");
	str = regex_replace(str,rgx1,"");
	str = regex_replace(str,rgx2,"");
}

// Convert image with uchar to single channel of float type with the option of scaling the intenstity from 255 to 1.
// Note: img_uchar can be gray or color image with uchar type. The return image has the type of float.
// it need to deallocate externally.
IplImage *createFloatGray_fromUcharImage(const IplImage *img_uchar, bool scaleToOne)
{
  // Check we have been supplied a non-null img pointer
  if (!img_uchar)
  {
	  printf("Error: img_uchar is NULL.\n");
	  exit(-1);
  }

  IplImage* gray, * floatGray;

  floatGray = cvCreateImage( cvGetSize(img_uchar), IPL_DEPTH_32F, 1 );

  if( img_uchar->nChannels == 1 )
    gray = (IplImage *) cvClone( img_uchar );
  else {
    gray = cvCreateImage( cvGetSize(img_uchar), IPL_DEPTH_8U, 1 );
    cvCvtColor( img_uchar, gray, CV_BGR2GRAY );
  }

  if (scaleToOne)
	cvConvertScale( gray, floatGray, 1.0 / 256.0, 0 );
  else
	cvConvertScale( gray, floatGray, 1.0, 0 );

  cvReleaseImage( &gray);
  return floatGray;
}

int f_Round(float x)
{
	int y;
	if (x>=0)
		y = int(x+0.5);
	else
		y = int(x-0.5);
	
	return y;
}

int dRound(double x)
{
	int y;
	if (x>=0)
		y = int(x+0.5);
	else
		y = int(x-0.5);
	
	return y;
}

//Get the atan angle of the point (x,y) in the range of [0,2pi)
float get_atan(float x, float y)
{
	if(x > 0 && y >= 0)
		return atan(y/x);
	
	if(x < 0 && y >= 0)
		return PI - atan(-y/x);
	
	if(x < 0 && y < 0)
		return PI + atan(y/x);
	
	if(x > 0 && y < 0)
		return 2*PI - atan(-y/x);

	if (x==0 && y>0)
		return PI/2.0f;

	if (x==0 && y<0)
		return 3.0f*PI/2.0f;
	
	return 0; //x==0 && y==0
}

float getMax(float x, float y)
{
	if (y>x)
		return y;
	else 
		return x;
}


int getMaxIndx(float* arr, int nElem)
{
	float maxVal = -1000000000.0f;
	int maxIndx = -1;

	for (int i=0;i<nElem;i++)
	{
		if (arr[i]>maxVal)
		{
			maxVal = arr[i];
			maxIndx = i;
		}
	}
	
	return maxIndx;
}

//histV has the dimension of 1 X nBin with the type of either CV_32SC1 (int) or CV_32FC1 (float)
//the return L2 normalized histV_L2 has the dimension of 1 X nBin with the type of CV_32FC1.
cv::Mat getL2Norm(cv::Mat histV)
{
	if (histV.rows !=1)
	{
		printf("Error: histV need to be a row vector.\n");
		exit(-1);
	}

	int nBin = histV.cols;
	int type = histV.type();

	cv::Mat histV_L2(1,nBin,CV_32FC1);
	float *histV_L2_row = histV_L2.ptr<float>(0);

	if (type == CV_32SC1)
	{
		int *histV_row = histV.ptr<int>(0);

		float sqrSum = 0.0f;
		for (int i=0;i<nBin;i++)
			sqrSum += (float) histV_row[i]*histV_row[i];

		float length = sqrt(sqrSum);
		
		for (int i=0;i<nBin;i++)
			histV_L2_row[i] = (float) histV_row[i]/length;
	}
	else if (type==CV_32FC1)
	{
		float *histV_row = histV.ptr<float>(0);

		float sqrSum = 0.0f;
		for (int i=0;i<nBin;i++)
			sqrSum += histV_row[i]*histV_row[i];

		float length = sqrt(sqrSum);
		
		for (int i=0;i<nBin;i++)
			histV_L2_row[i] = histV_row[i]/length;
	}

	return histV_L2;
}

//perform pow(base,exponent) for each element in exponentM. exponenentM has either CV_32FC1 and CV_64FC1 type.
cv::Mat getPowMat(double base, cv::Mat exponentM)
{
	cv::Mat outM(exponentM.size(),exponentM.type());
	
	if (exponentM.type() == CV_32FC1) //float type
	{
		for (int i=0;i<exponentM.rows;i++)
		{
			float *exponentM_row = exponentM.ptr<float>(i);
			float *outM_row = outM.ptr<float>(i);
			
			for (int j=0; j<exponentM.cols;j++)
			{
				outM_row[j] = pow((float) base, exponentM_row[j]);
			}
		}
	}
	else if (exponentM.type() == CV_64FC1) //double type
	{
		for (int i=0;i<exponentM.rows;i++)
		{
			double *exponentM_row = exponentM.ptr<double>(i);
			double *outM_row = outM.ptr<double>(i);
			
			for (int j=0; j<exponentM.cols;j++)
			{
				outM_row[j] = pow(base, exponentM_row[j]);
			}
		}
	}
	else 
	{
		printf("Error: only surpport CV_32FC1 and CV_64FC1 type for exponentM.\n");
		exit(-1);
	}
	
	return outM;
}

//find first vector index of elem in vec.
int findElemIndx(vector<float> vec, float elem)
{
	int indx = -1;

	for (int i=0;i<(int) vec.size();i++)
	{
		if (vec[i]==elem)
		{
			indx = i;
			break;
		}
	}

	return indx;
}

//convert int to string
string int2Str(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

//search the first sub-string which match regexMatch_str, and get matched substrings in all parenthesis of regular expression.
//str = "C:\\boost_1_47_0\\boost\\any\\any.hpp";
//regexMatch_str = "\\W(\\w+)\\.(\\w+)$";
vector<string> getMatchedSubStrings(string str, string regexMatch_str)
{
	vector<string> matched_subStrings;

	regex reg(regexMatch_str);

	match_results<string::const_iterator> matches;

	if (regex_search(str, matches, reg))
	{
		match_results<string::const_iterator>::const_iterator i;
		for (i=matches.begin()+1;i<matches.end();i++)
		{
			matched_subStrings.push_back(i->str());
		}
	}
	else
	{
		printf("Warning: no matched found in (%s) by using the regular expression (%s).\n",str.c_str(),regexMatch_str.c_str());
	}

	return matched_subStrings;
}

//get base name from a file name with or without path
string getFileBaseName(string file)
{
	string regexMatch_str = "\\W*(\\w+)\\.\\w+$";
	vector<string> baseNames = getMatchedSubStrings(file, regexMatch_str);

	if (baseNames.empty())
	{
		printf("Error: failed to get file base name.\n");
		exit(-1);
	}

	return baseNames[0];
}