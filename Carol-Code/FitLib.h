// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the FITLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// FITLIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#include <vector>
#include "asmfitting.h"
#ifdef FITLIB_EXPORTS
#define FITLIB_API __declspec(dllexport)
#else
#define FITLIB_API __declspec(dllimport)
#endif

typedef struct MLFaceAAMPoints_tag 
{
public:
	int m_iTotalNum; //total number of points
	int m_iFrame; //Frame number in the video
	int m_iCenterX; //point 68 -- middle point of nose
	int m_iCenterY; //point 68 -- middle point of nose
	int m_iMinX; // minimum x in all the points of face shape.
	int m_iMinY; // minimum y in all the points of face shape.
	int m_iMaxX; // maximum x in all the points of face shape.
	int m_iMaxY; // maximum y in all the points of face shape.
	CPoint m_points[68];
} MLFaceAAMPoints;

FITLIB_API void* ASMFitCreate();
FITLIB_API void ASMFitRelease(void *pASMFit);
FITLIB_API int ASMFitLoadModel(void *pASMFit, const char* pczModelFileName);
FITLIB_API int ASMFitLoadCascade(const char* pczCascadeFileName);
FITLIB_API int ASMFitProcessImage(void *pASMFit, unsigned char* pczImgData, int iImgWidth, int iImgHeight, int iImgWdthStep, 
								  const char* pczImgType, int iFrameNum, int iNumiteration,
								  MLFaceAAMPoints* face_shape);