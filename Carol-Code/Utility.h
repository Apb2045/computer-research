/*********************************************************************
 *File: Utility.h
 *Purpose: all utility functions
 *
 *Author: Shizhi Chen
 *Email: schen21@ccny.cuny.edu
 *Date: 4/4/2012
 *********************************************************************/

#ifndef Utility_H___
#define Utility_H___

#include "initializefile.h"

//check if file in dir director exist.
bool isFileExist(string dir,string file);

//grap all files from a folder specified by "dir", which matched regular expression of "matchRegExpStr"
//then stored all files in the folders to a vector<string> "files"
//Note: need '\\' in c++ to represent '\' in perl regular expression, e.g. "\\.jpg$", 
bool grapFiles (string dir,string matchRegExpStr, vector<string> &files);

//trim the leading and trailing space of the input string.
void trimString(string& str);

// Convert image with uchar to single channel of float type with the option of scaling the intenstity from 255 to 1.
// Note: img_uchar can be gray or color image with uchar type. The return image has the type of float.
// it need to deallocate externally.
IplImage *createFloatGray_fromUcharImage(const IplImage *img_uchar, bool scaleToOne);

int f_Round(float x);

int dRround(double x);

//Get the atan angle of the point (x,y) in the range of [0,2pi)
float get_atan(float x, float y);

float getMax(float x, float y);

int getMaxIndx(float* arr, int nElem);

//histV has the dimension of 1 X nBin with the type of either CV_32SC1 (int) or CV_32FC1 (float)
//the return L2 normalized histV_L2 has the dimension of 1 X nBin with the ype of CV_32FC1.
cv::Mat getL2Norm(cv::Mat histV);

//perform pow(base,exponent) for each element in exponentM. exponenentM has either CV_32FC1 and CV_64FC1 type.
cv::Mat getPowMat(double base, cv::Mat exponentM);

template<typename T, size_t N>
T* endOfArray(T (&ra)[N])
{
	return ra + N;
}

template<typename T, size_t N>
size_t getArrSize(T (&ra) [N])
{
	return N;
}

//find first vector index of elem in vec.
int findElemIndx(vector<float> vec, float elem);

//convert int to string
string int2Str(int number);

//search the first sub-string which match regexMatch_str, and get matched substrings in all parenthesis of regular expression.
//str = "C:\\boost_1_47_0\\boost\\any\\any.hpp";
//regexMatch_str = "\\W(\\w+)\\.(\\w+)$";
vector<string> getMatchedSubStrings(string str, string regexMatch_str);

//get base name from a file name with or without path
string getFileBaseName(string file);

#endif

