///*Authors : D. Michael Quintian, YingLi Tian
//2010 - 2011
//*/
//
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>

// local project
#include "ASMModelFit.h"

using namespace std;

void formatFileName(const char* in_name, char* out_name);
////**************************************************************************
//
void DrawCircleFeature(IplImage* pInImg, int ptx, int pty, int sc, int inColor)
{
	CvScalar color; 
	switch(inColor)
	{
	case 0:
		color = CV_COLOR_RED;
		break;
	case 1:
		color = CV_COLOR_GREEN;
		break;
	case 2:
		color = CV_COLOR_YELLOW;
		break;
	case 3:
		color = CV_COLOR_BLUE;
		break;
	default:
		color = CV_COLOR_YELLOW;
	}

	int radius=1;//(int)floor(1.0*sqrt((double)sc));
	int thickness = -1;
	CvPoint pt;
	pt.x = ptx;
	pt.y = pty;
	cvCircle(pInImg, pt, radius, color, thickness);
}
//
////************************************************************************

//  This function is a huge mess.  you probably don't need it, or you can
//  use it as starting point for appearance based processing
void edge(IplImage *image)
{	

	IplImage *smooth = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,1);
	IplImage *ycbcr = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,3);
	IplImage *planes[3] = {0,0,0};

	for( int i = 0; i < 3; i++ )
                planes[i] = cvCreateImage( cvGetSize(image), 8, 1 );
	
	cvSmooth(image,ycbcr,CV_GAUSSIAN,5,5);
	cvCvtColor(ycbcr,image,CV_RGB2YCrCb);
	cvSplit(image,planes[0],planes[1],planes[2],0);

	cvReleaseImage(&ycbcr);

	//cvEqualizeHist(planes[2],smooth);
	//cvThreshold(smooth,planes[2],140,255,CV_THRESH_TOZERO);
	//cvThreshold(planes[2],smooth,200,255,CV_THRESH_TOZERO_INV);

	cvThreshold(planes[2],smooth,140,255,CV_THRESH_TOZERO);
	cvThreshold(smooth,planes[2],150,255,CV_THRESH_TOZERO_INV);

	cvThreshold(planes[1],smooth,0,255,CV_THRESH_TOZERO);
	cvThreshold(smooth,planes[1],255,255,CV_THRESH_TOZERO_INV);


	cvZero(planes[0]);
	//cvZero(planes[1]);

	cvMerge(planes[0],planes[1], planes[2],0,image);
	
	cvReleaseImage(&smooth);
}

void saveMouthRegionImage(IplImage* image,char* file,int count)
{
	char name[50];
	char number[50];
	
	formatFileName(file,name);
	sprintf(number,"\window\\%s%i.jpg",name,count);

	cvSaveImage(number,image);

}

//  This is so you can extract the name of the video file without 
//  the path or file extension.  There are probably better ways to do this
//  Note that in SSCANF, you need to enter the path where the videos are located

void formatFileName(const char* in_name, char* out_name)
{
	char temp[64];

	// Set the path to where the video is located, to parse the filename
	// into IN_NAME
	sscanf(in_name,"..\\videos\\words\\%s", out_name);//temp);
	char* pavi;
	pavi = strstr(out_name/*temp*/,".");
	strncpy(pavi,"",1);

}
//  read the header
asm_shape getMouthFromShape(asm_shape v)
{
	asm_shape mouth;

	mouth.Resize(19);

	for(int i=0;i<19;i++)
	{
		mouth[i].x = v[i+48].x;
		mouth[i].y = v[i+48].y;
	}
	

	return mouth;

}
