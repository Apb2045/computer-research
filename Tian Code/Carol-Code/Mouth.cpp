/*Authors : Carol Mazuera, YingLi Tian
2012 - 2013
*/

#include <stdio.h>
#include "stdlib.h"
#include <string>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>


// local project

#include "Mouth.h"

using namespace std;


static int count1 = 0;
float NormDistance1 = 0.0;
float NormDistance2 = 0.0;
float nWidth = 0.0;
float nUpper = 0.0;
float nLower = 0.0;
int max_num=0;

//double diff[][19];
vector< vector<float> > Dist;
//vector<string> sub;
vector<string> wrd;
vector< vector<int> > frames;
vector<int> groundTruth_labes(2,0); //FOr SVM use
vector< vector<int> > feat ;
//vector<int> moving_frames(2,0);
//vector<int> neutral_frames(2,0);
//vector<int> thresh(2,0);
int frame_count = 0;

Mouth::Mouth(){

//    diff[] = 0;
    mouth.Resize(19);
//    count;
    NormDistance1;
    NormDistance2;
    frame_count;
//    diff;
    mouthN.Resize(19); // CAM
}

//************************************************

CvPoint Mouth::AddShape(asm_shape eshape){


    for(int i=0;i<19;i++)
    {
        mouth[i].x = eshape[i+48].x;
        mouth[i].y = eshape[i+48].y;
    }

    double x,y;
    mouth.COG(x,y);

    CvPoint center;
    center.x = (int)x;
    center.y = (int)y;

    return  center;
}

//***************************************************

void Mouth::SetFrameResize(int f, int r){

    frame_count = f;
    resize = r;

printf("%i",frame_count);
    shapes = new asm_shape[frame_count];
    features = new MouthFeatures[frame_count];
 
}

//******************************************************** CAM

void Mouth::Align()
{
    mouth = center_rotate(mouth);
}

//********************************************************* CAM
void Mouth::shape2file(asm_shape eshape, char* file){

	//extracts name of video file
	char* pch1, *pch3, *hold_word, *hold_word2;
	hold_word = new char [20];
//	hold_word2 = new char [20];
	pch1 = strrchr(file, '/');
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0'; 
cout<<hold_word<<endl;
	//creates two strings for file naming
//	strcpy(hold_word2, hold_word);
cout<<"strcpy"<<endl;
	strcat(hold_word, "_mouthp.txt");
cout<<"_bp.txt"<<endl;
//	strcat(hold_word2, "_allp.txt");
cout<<"_allp.txt"<<endl;
//	strcat(hold_word2, "_bp.txt");
cout<<"_mouthp.txt"<<endl;

	ofstream allpoints;
cout<<"allpoints"<<endl;
//	ofstream bounding_points;
cout<<"bounding_points"<<endl;
	allpoints.open(hold_word,ios::app);
cout<<"allpoints.open"<<endl;
//	bounding_points.open(hold_word2,ios::app);
	
/*	//This is to save all 68 landmarkpoints with no alignment or normalization
    for(int i=0;i<67;i++)
    {
		allpoints<<eshape[i].x<<"\t"<<eshape[i].y<<"\t";
	}
	allpoints<<endl;
*/
cout<<"before for loop"<<endl;
	for(int i=0;i<19;i++)
    {
		allpoints<<mouth[i].x<<"\t"<<mouth[i].y<<"\t";
	}
	allpoints<<endl;
cout<<"after for loop"<<endl;
/*	//bounding box points ONLY <left, rigth, top, bottom>
	bounding_points<<eshape[4].x<<"\t"<<eshape[4].y<<"\t"<<eshape[10].x<<"\t"<<eshape[10].y<<"\t"
		<<eshape[41].x<<"\t"<<eshape[41].y<<"\t"<<eshape[7].x<<"\t"<<eshape[7].y<<endl;
*/
	allpoints.close();
//	bounding_points.close();

//	delete[] hold_word;

//	delete[] hold_word2;
cout<<"close file"<<endl;
}

//********************************************************

asm_shape Mouth::center_rotate(asm_shape m)
{
    float x0, y0, x6, y6, theta;

    asm_shape temp;
    temp.Resize(19);
    temp = m;

    x0 = m[0].x;
    y0 = m[0].y;

    x6 = m[6].x;
    y6 = m[6].y;

    // Find the angle to rotate mouth shape
    // so that landmark 1 and 19 have the same Y coordinate
    theta = -atan((y6-y0)/(x6-x0));

    // Get mouth points from shape data and rotate them.
    for(int i=0;i<19;i++)
    {
        m[i].x = temp[i].x*cos(theta)-temp[i].y*sin(theta);
        m[i].y = temp[i].x*sin(theta)+temp[i].y*cos(theta);
    }

    //  Translates the shape, places mean X and Y values at the origin.
    m.Centralize();

    float y1 = m[0].y;
	float x1 = m[0].x;

/*ofstream Outfile;
Outfile.open("AlignCenter.txt",ios::app);
 */   for(int i=0;i<19;i++)
    {
        m[i].y = m[i].y - y1;   //  Then shift up or down so that y0 and y6 = 0
//		m[i].x = m[i].x - x1;   //  Then shift left so that x0 = 0
//m[i].y = m[i].y + 40;
 //       Outfile<<(m[i].x)<<"\t\t"<<m[i].y<<endl;
    }
 //   Outfile.close(); 

    return m;

}

//******************************************************************* CAM
void Mouth::NormalizeVidSeg(int frame, char* file)
{
// MIKEs Normalization ///////////////////////////////////////
/*
	int i; float x=0, y=0, l=0, li=0, m=0, x0=0, Width=0, Upper=0, Lower=0;

	//extracts name of video file
	char* pch1, *pch3, *hold_word;
	hold_word = new char [45];
	pch1 = strrchr(file, '/'); //for use with Carol's dataset
//	pch1 = strrchr(file, '\\'); // for use with Mike's dataset
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0'; 

	//creates string for file naming
//	strcat(hold_word, "mouth.txt");
	strcat(hold_word, ".txt");
ofstream Outfile;
//Outfile.open("Normalized.txt",ios::app);
Outfile.open(hold_word,ios::app);

if (frame == 0)
{
	NormalizeNeutral();
} else
{
	NormalizeM(); 
}
for(i =0; i<19; i++)
    {
		Outfile<<mouth[i].x<<'\t'<<mouth[i].y<<'\t';
    }
	
	setNeutralFeatures(mouth,&measures);

	Width = (float)measures.NeutralWidth;
	Upper = (float)measures.NeutralHeightUpper;
	Lower = (float)measures.NeutralHeightLower;

	Outfile<<Width<<'\t'<<Upper<<'\t'<<Lower<<endl;
*/

//MY NORMALIZATION TECHNIQUE //////////////////////////////////
	int i; float x=0, y=0, li=0, m=0, x0=0, Width=0, Upper=0, Lower=0;
	float l;

	//extracts name of video file
	char* pch1, *pch3, *hold_word;
	hold_word = new char [45];
	pch1 = strrchr(file, '/'); //for use with Carol's dataset
//	pch1 = strrchr(file, '\\'); // for use with Mike's dataset
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0'; 

	//creates string for file naming
	strcat(hold_word, ".txt");
ofstream Outfile;
Outfile.open(hold_word,ios::app);

	l = sqrt(pow(mouth[6].x, 2) + pow(mouth[6].y, 2));
    for(i =0; i<19; i++)
    {
		li = sqrt(pow(mouth[i].x, 2) + pow(mouth[i].y, 2));
		m = mouth[i].y/mouth[i].x;
		x = (li * 0.5)/(l*sqrt(pow(m,2) + 1));
		y = (m*li*0.5)/(l*sqrt(pow(m,2) + 1));

		mouth[i].x <0 ? mouth[i].x = -x : mouth[i].x = x;
		mouth[i].x <0 ? mouth[i].y = -y : mouth[i].y = y;
//		Outfile<<mouth[i].x<<'\t'<<mouth[i].y<<'\t';
    }
//	Outfile<<endl;
	x0 = mouth[0].x;
	for(i =0; i<19; i++)
	{
		mouth[i].x = mouth[i].x - x0;
		Outfile<<mouth[i].x<<'\t'<<mouth[i].y<<'\t';
	}
		
	setNeutralFeatures(mouth,&measures);

	Width = (float)measures.NeutralWidth;
	Upper = (float)measures.NeutralHeightUpper;
	Lower = (float)measures.NeutralHeightLower;

	Outfile<<Width<<'\t'<<Upper<<'\t'<<Lower<<endl;

delete[] hold_word; 
Outfile.close();


// CHECK WHAT THIS IS FOR !!!
/*
		int i; double Norm;
		NormDistance1 = 1/mouth[6].x; //Scaling factor

ofstream Outfile;
Outfile.open("../../Normalized.txt",ios::app);

    //scale lip shape
	mouth.Scale(NormDistance1);

    for(i =0; i<19; i++)
    {
		Outfile<<mouth[i].x<<"\t\t"<<(mouth[i].y)*(-1)<<endl;
    }
Outfile.close();
count1 = 1; // CHECK WHAT THIS IS FOR
*/
/*
	int i; double Norm;
    if (count1 == 0) //first frame of the first video
    { //first frame's eyes distance and mouth landmarks
        NormDistance1 = sqrt(pow((eshape[29].x - eshape[34].x),2) + pow((eshape[29].y - eshape[34].y),2));
        NormDistance2 = sqrt(pow((eshape[27].x - eshape[32].x),2) + pow((eshape[27].y - eshape[32].y),2));
        mouthN = mouth;
    }
ofstream Outfile;
Outfile.open("../../Normalized.txt",ios::app);

    //scale frame to first normal frame using first frame's eyes distance
    Norm = (sqrt(pow((eshape[29].x - eshape[34].x),2) + pow((eshape[29].y - eshape[34].y),2)))/NormDistance1;
    for(i =0; i<19; i++)
    {
        mouth[i].x = mouth[i].x / Norm;
        mouth[i].y = mouth[i].y / Norm;

//        mouth[i].y = mouth[i].y / mouthN[i].y;
Outfile<<(mouth[i].y)*(-1)<<"\t\t"<<mouth[i].x<<endl;
    }
Outfile.close();
count1 = 1; 
*/
}


//******************************************************************* CAM
void Mouth::TFrames(int g)
{
//    int hold=0;
    ofstream Outfile;
    Outfile.open("Temp.txt", ios::trunc);
    if(Outfile.fail()) cout<<"file didnt open"<<endl;
    Outfile<<g;
    Outfile.close();

    string frame;
    ifstream Infile ("Temp.txt");
    Infile.is_open();
        getline (Infile,frame);
        frame_count = atoi(frame.c_str());
 
        Infile.close();

}
 


//******************************************************************* CAM
void Mouth::LandDist(int f)
{
//TFrames(f); 
    int i, j, h=0; 
    vector<float> hold;

//    ofstream Outfile;
//    Outfile.open("../../CAROLDIFF.txt",ios::app);
//    if(Outfile.fail()) cout<<"file didnt open"<<endl;

    for (i=1; i<6; i++)
    {
        for (j=6; j<13; j++)
        {
            if (j == 12)
            {
                hold.push_back(sqrt(pow((mouth[i].x - mouth[0].x),2) + pow((mouth[i].y - mouth[0].y),2)));
            }
            else
            {
                hold.push_back(sqrt(pow((mouth[i].x - mouth[j].x),2) + pow((mouth[i].y - mouth[j].y),2)));
            }
//            Outfile<<hold[h]<<"\t";
            h++;
        }
    }
	hold.push_back(sqrt(pow((mouth[6].x - mouth[0].x),2) + pow((mouth[6].y - mouth[0].y),2))); //horizontal distance between mouth corners 
//	Outfile<<hold[h]<<endl;

    Dist.push_back(hold);
//    Outfile.close();
}

//******************************************************************* CAM
void Mouth::GroundThruth()
{
    string str1, str2; vector<int> frame; int i, j, hold_frame;

    ifstream Infile;
    Infile.open("FramesSpace.txt");
    if(Infile.fail()) 
	{
		cout<<"FramesSpace.txt File didnt open"<<endl;
		exit(1);
	}
    for (i=0; i<220; ++i)
    {
        Infile>>str1>>str2;// copies word then subject number
		str1.append(str2);
        wrd.push_back(str1);
       
        for(j=0; j<10; j++)
        {
            Infile>>hold_frame;
            frame.push_back(hold_frame);
        }
        frames.push_back(frame);
        frame.clear();
	}

/*   for(unsigned k=0; k<wrd.size(); ++k)
    {
        cout<<sub.at(k)<<endl;
        cout<<wrd.at(k)<<endl;
        for(unsigned l=0; l<frames[1].size(); ++l)
        {
            cout<<frames[k].at(l);
        }
        cout<<endl; 
    }
*/
    Infile.close();
}


//********************************************************************* CAM
void Mouth::Dynamics_Recog(char* file)
{	
	char* pch1, *pch3, *hold_word;
	unsigned j, i, k, last_row = Dist.size(), row_size = Dist[1].size(); int b=0, c=0;
	float col=0;// hold_sum=0;
    vector<float> hold;// vector< vector<float> > hold_diff; 
	
	//Calculate neutral average frame of the 1st 5 neutral frames of the first video for one subject
	for (j=0; j<Dist[0].size(); j++)
    {
        hold.push_back((Dist[0][j]+Dist[1][j]+Dist[2][j]+Dist[3][j]+Dist[4][j])/5);
    }
    Dist.push_back(hold);
    hold.clear();

	//extracts name of video file
	hold_word = new char [45];
	pch1 = strrchr(file, '/'); //for use with Carol's dataset
//	pch1 = strrchr(file, '\\'); // for use with Mike's dataset
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0'; 

	//creates string for file naming
	strcat(hold_word, ".txt");
	ofstream Outfile;
	Outfile.open(hold_word);

	//Calculates the difference btw neutral average frame and current frame
    for (j=0; j<last_row; j++)
    {
        for (i=0; i<row_size; i++)
        {
			Outfile<<(Dist[j][i]-Dist[last_row][i])<<"\t";
        }
		Outfile<<endl;
    }
	Dist.clear();
	Outfile.close();
	delete[] hold_word;
}

//******************************************************************* CAM
void Mouth::Hist(char* file)
{	
	char* pch1, *pch3, *hold_word;
	unsigned j, i, k, last_row = Dist.size(), row_size = Dist[1].size(); int b=0, c=0;
	float col=0, hold_sum=0;
    vector<float> hold;// vector< vector<float> > hold_diff; 
	
//	GroundThruth();
	
	//Calculate neutral average frame of the 1st 5 neutral frames of the first video for one subject
	for (j=0; j<Dist[0].size(); j++)
    {
        hold.push_back((Dist[0][j]+Dist[1][j]+Dist[2][j]+Dist[3][j]+Dist[4][j])/5);
    }
    Dist.push_back(hold);
    hold.clear();
/*
// FOR RECOGNITION DYNAMICS

	//extracts name of video file
	hold_word = new char [45];
	pch1 = strrchr(file, '/'); //for use with Carol's dataset
//	pch1 = strrchr(file, '\\'); // for use with Mike's dataset
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0'; 

	//creates string for file naming
//	strcat(hold_word, "mouth.txt");
	strcat(hold_word, ".txt");
ofstream Outfile;
//Outfile.open("Normalized.txt",ios::app);
Outfile.open(hold_word);

	//Calculates the difference btw neutral average frame and current frame

    for (j=0; j<last_row; j++)
    {
        for (i=0; i<row_size; i++)
        {
			Outfile<<(Dist[j][i]-Dist[last_row][i])<<"\t";
//            hold_sum = hold_sum + (Dist[j][i]-Dist[last_row][i]); 
        }
//        hold_diff.push_back(hold);//holds diference values to implement histogram
//        hold.push_back(hold_sum);
		Outfile<<endl;
//		hold_sum = 0;
    }
	Dist.clear();
	Outfile.close();
	delete[] hold_word;
*/
// FOR SEGMENTATION DYNAMICS ///////////////////////////////////////
		//Calculates the difference btw neutral average frame and current frame
	ofstream outfile;
	outfile.open("Features_train_S05.txt",ios::app);
    for (j=0; j<last_row; j++)
    {
        for (i=0; i<row_size; i++)
        {
//			outfile<<((Dist[j][i]-Dist[last_row][i]) * 10)<<"\t";
            hold_sum = hold_sum + (Dist[j][i]-Dist[last_row][i]); 
//			if (max_num < int(abs(hold[i])+0.5)) max_num=int(abs(hold[i])+0.5);
        }
//        hold_diff.push_back(hold);//holds diference values to implement histogram
        hold.push_back(hold_sum);
		outfile<<hold_sum<<endl;
		hold_sum = 0;
    }
	Dist.clear();
	outfile.close();


	//separates moving frames from neutral frames
	hold_word = new char [15];
	pch1 = strrchr(file, '/');
	pch3 = strrchr(file, '.');
	int a = (pch3 - pch1)-1;	
	strncpy(hold_word, pch1 + 1, a); //extracts name of video file
	hold_word[a]='\0';
	ofstream outfile2;	
	outfile2.open("GroudT_train_S05.txt",ios::app);
	for(i=0; i<wrd.size(); i++)
	{cout<<"hold_word = "<<hold_word<<endl;
		char *compare = (char*)wrd[i].c_str();//cout<<"compare = "<<compare<<endl;
		
		if(strcmp(hold_word, compare) == 0)
		{ 
			for(j=0; j<hold.size(); j++)
	 		{
				if((j>(frames[i][0]+5) && j<(frames[i][1])-5) || (j>(frames[i][2]+5) && j<(frames[i][3]-5)) || 
					(j>(frames[i][4]+5) && j<(frames[i][5]-5)) || (j>(frames[i][6]+5) && j<(frames[i][7]-5) ||
					(j>(frames[i][8]+5) && j<(frames[i][9]-5))))
				{
					//assigns a "1" value if frame is moving
//				    groundTruth_labes[j] = 0.0;
					outfile2<<"1"<<endl;
					b++;
				}
				else
				{
					//assigns a "0" value if frame is neutral
//					groundTruth_labes[j] = 1.0;
					outfile2<<"0"<<endl;
					c++;
				}
			} 
			break;
		}else{}
	}
	outfile2.close();
	cout<<"moving frames = "<<b<<endl;
	cout<<"neutral frames = "<<c<<endl;
	hold.clear();
	delete[] hold_word;

/*	cv::Mat trainLabels_true(groundTruth_labes);


	int nFold = 5; //number of folds to separate the database
	int foldIndx_test = 4; //0-based

	//svm
	int nFold_cv = 5;
	cv::Mat log2gs = (cv::Mat_<float>(1,1) <<  -4); //best log2g ~= -4 (sift, fastSift); -3(surf); 
	cv::Mat log2cs = (cv::Mat_<float>(1,1) <<  4); //best log2c ~= 4 (sift, fastSift); 3(surf);

	bool scaleFlag =true; 
	char str[3];
	_itoa(foldIndx_test, str, 10);
	string model_file ("svm_");	model_file += str; model_file.append(".model");
	string train_xmlFile ("train_"); train_xmlFile += str;	train_xmlFile += ".xml";
	float scale_min =0.0f;
	float scale_max =1.0f; 
	
	Svmlib_c svm(scaleFlag,model_file, train_xmlFile, scale_min, scale_max, nFold_cv);

	svm.train_RBF(histogram_train, trainLabels_true, log2gs, log2cs);
*/
}


//****************************************************************** CAM
void Mouth::Draw_Hist(char* file)
{
char* pch1, *pch3, *hold_word;
	unsigned j, i, k, last_row = Dist.size(), row_size = Dist[1].size(); int col=0, hold_sum=0, b=0, c=0;
    vector<float> hold; vector< vector<float> > hold_diff; 
	//extracts name of video file
	hold_word = new char [15];
//	pch1 = strrchr(file, '/');
	pch3 = strrchr(file, '.');
	char keys[] = ".";
	int a = strcspn (file,keys);
//	int a = (pch3 - pch1)-1;
//	int a = pch3 - 1;
//	strncpy(hold_word, pch1 + 1, a); 
	strncpy(hold_word, file, a); 
	hold_word[a]='\0';
	strcat (hold_word,".txt");
	
//	GroundThruth();
	
	//Calculate neutral average frame of the 1st 3 neutral frames of the first video for one subject
for (j=0; j<Dist[0].size(); j++)
    {
        hold.push_back((Dist[0][j]+Dist[1][j]+Dist[2][j])/3);
    }
    Dist.push_back(hold);
    hold.clear();

		//Calculates the difference btw neutral average frame and current frame
	ofstream outfile;
	outfile.open(hold_word);
    for (j=0; j<last_row; j++)
    {
        for (i=0; i<row_size; i++)
        {
			outfile<<((Dist[j][i]-Dist[last_row][i]))<<"\t";
  //          hold.push_back((Dist[j][i]-Dist[last_row][i]) * 10); //SVM use only
//			if (max_num < int(abs(hold[i])+0.5)) max_num=int(abs(hold[i])+0.5);
        }
//        hold_diff.push_back(hold);//holds diference values to implement histogram
 //       hold.clear(); 
		outfile<<endl;
    }
	Dist.clear();
	outfile.close();
	




/////////////////////////////////////////////////////////////////////////////

/*
	//Finds best threshold value
	int T = int((thresh.size()/2) + 0.5);
	int hold_new_T =0, m1=0, m2=0,b=0, c=0, a=0, d=0,i;
	if(T =! hold_new_T)
	{	
		if(hold_new_T != 0)	T = hold_new_T;
cout<<"HIST "<<T<<endl;
		for(i=0; i<thresh.size(); i++)
		{ cout<<"for loop"<<endl;
			if(thresh.at(i) <= T)
			{cout<<"if loop"<<endl;
				b += thresh[i];
				if(i==0)
				{
					a += thresh[i];
				}
				else 
				{
					a += (thresh[i] * i);
				}
			}
			else if(thresh.at(i) > T)
			{cout<<"else if loop"<<endl;
				c += thresh[i];
				d += (thresh[i] * i);
			}else{}
		}
		m1 = a/b; m2 = d/c;
		hold_new_T = (m1 + m2)/2;
	}
	
	cout<<"Best Threshold= "<<T<<endl;
*/

//Draw rudimentary dotted histogram on a txt file
/*	int j;
	ofstream Outfile; 
    Outfile.open("../../Hist.txt");

	for(j=0; j<thresh.size(); j++)
    {
		Outfile<<j<<"  "<<thresh.at(j)<<endl;

   //     Outfile<<neutral_frames.at(j)<<"\t";
	//	Outfile<<moving_frames[j]<<"\n";
    }
*/	
	//draws the dotted histogram
/*    for(j=0; j<moving_frames.size(); j++)
    {
        Outfile<<j;
        for (i=0; i<moving_frames[j]; i++)
        {
            Outfile<<".";
        }
        Outfile<<endl;
    }
	Outfile<<endl;Outfile<<endl;
	 for(j=0; j<neutral_frames.size(); j++)
    {
        Outfile<<j;
        for (i=0; i<neutral_frames[j]; i++)
        {
            Outfile<<".";
        }
        Outfile<<endl;
    }
*/
 //   Outfile.close();
}


//********************************************************************

void Mouth::NormalizeNeutral()
{

    mouth = center_rotate(mouth);

    setNeutralFeatures(mouth,&measures);

	nWidth = (float)measures.NeutralWidth;
	nUpper = (float)measures.NeutralHeightUpper;
	nLower = (float)measures.NeutralHeightLower;


    for(int i=0;i<19;i++)
    {
        mouth[i].x = mouth[i].x/nWidth;
        if(mouth[i].y >= 0)
        {
            mouth[i].y = mouth[i].y/nUpper;  //upper
        }
        else { mouth[i].y = mouth[i].y/nLower; } //lower

        //printf("\nLandmark %i X = %f, Y = %f ",i,shape[i].x, shape[i].y);
    }
 /*   mouth.Normalize();

    setFeatures(mouth,&measures);
    features[count1] = measures;
    shapes[count1] = mouth;

	count1++;
*/
	}

//***************************************************************************

void Mouth::NormalizeM()
{

    mouth = center_rotate(mouth);


    for(int i=0;i<19;i++)
    {
        mouth[i].x = mouth[i].x/nWidth;
        if(mouth[i].y >= 0)
        {
            mouth[i].y = mouth[i].y/nUpper;  //upper
        }
        else { mouth[i].y = mouth[i].y/nLower; } //lower

        //printf("\nLandmark %i X = %f, Y = %f ",i,shape[i].x, shape[i].y);
    }
 /*   mouth.Normalize();

    setFeatures(mouth,&measures);
    features[count1] = measures;
    shapes[count1] = mouth;

    count1++;
 */

}
//************************************************************************

void Mouth::setNeutralFeatures( asm_shape mouth, MouthFeatures *m_feature)
{

    m_feature->NeutralHeightUpper   = mouth[0].y - mouth.MinY();
    m_feature->NeutralHeightLower = mouth.MaxY() - mouth[0].y;
    m_feature->NeutralWidth  = mouth.GetWidth(-1,-1);

    m_feature->Height = mouth.GetHeight();
    m_feature->Width  = mouth.GetWidth(-1,-1);
}

//**************************************************************************

void Mouth::setFeatures(asm_shape mouth, MouthFeatures* m_feature)
{
    //m_feature->Height = mouth.GetHeight();
    m_feature->Width  = mouth.GetWidth(-1,-1);

    m_feature->HeightU   = mouth[0].y - mouth.MinY();
    m_feature->Height = mouth.MaxY() - mouth[0].y;
}


//*****************************************************************************
// Linear Interpolation
void Mouth::FitTime(int start, int finish)
{
    new_size = new asm_shape[resize];
    newfeatures = new MouthFeatures[resize];
 

    double N = resize;
    double M = finish + 1 - start;

    int i, j, a, b;

    float x;

    for(i=0;i<N;i++)
    {
        new_size[i] = shapes[0];

        a = floor(i*M/N) + start;
        x = (i*M/N) - floor(i*M/N);
        b = ceil(i*M/N) + start;

        if(b<finish)
        {
            for(j=0;j<19;j++)
            {
                new_size[i][j].x = (1-x)*(shapes[a][j].x) + x*(shapes[b][j].x);
                new_size[i][j].y = (1-x)*shapes[a][j].y + x*shapes[b][j].y;
            }
            newfeatures[i].Height = (1-x)*features[a].Height + x*features[b].Height;
            newfeatures[i].Width = (1-x)*features[a].Width + x*features[b].Width;
            newfeatures[i].HeightU = (1-x)*features[a].HeightU + x*features[b].HeightU;
        }
        else
        {
            for(j=0;j<19;j++)
            {
                new_size[i][j].x = shapes[a][j].x ;
                new_size[i][j].y = shapes[a][j].y ;
            }
            newfeatures[i].Height = features[a].Height;
            newfeatures[i].Width = features[a].Width;
            newfeatures[i].HeightU = features[a].HeightU;
        }
    }
    delete[] shapes,features;
}

//***********************************************************************

void Mouth::WriteFrames(char* file)
{
    ofstream outfile;
 
    char filename[50];
    char temp[50];

    formatFileName(file,temp);//   Set where the text will go
//    sprintf(filename,"../../../MATLAB/words/%s%s",temp,".txt");
    sprintf(filename,"../VidSeg/%s%s",temp,".txt");
//cout<<"HEREEEEEEEEEEEEEEEEEE"<<endl;
    outfile.open(filename,ios::out);

    double M = resize, x, y;
    int i, k;

    for(i=0;i<M;i++)
    {
        for(k=0;k<19;k++)
        {
            outfile<<new_size[i][k].x<<"\t"<<new_size[i][k].y<<"\t";
        }

        outfile<<newfeatures[i].Height<<"\t"<<newfeatures[i].Width<<"\t"<<newfeatures[i].HeightU<<"\t";
        outfile<<endl;
    }
 
    delete[] new_size, newfeatures;
    outfile.close();

}

//***********************************************************************
//THIS ONE IS CAUSING FILENAME TO BECOME CORRUPTED WHEN TRYING TO COPY ITS CONTENT. I DONT KNOW THE REASON. (CAM)
void Mouth::formatFileName(const char* in_name, char* out_name)
{
    // Set the path to where the video is to parse
    // the path and extension off.1
//    sscanf(in_name,"..\\videos\\words\\mike\\%s", out_name);
//    char* pavi;
//    pavi = strstr(out_name/*temp*/,".");
//    strncpy(pavi,"",1);

//    char* pavi; char* hold[100];printf("HEEeeerEE ");cout<<in_name<<endl;
    //hold = in_name;
//    strcpy(hold,in_name);
//    pavi = strstr(hold,".avi");
//    strncpy(pavi," ",4);
//    sscanf(in_name,"%s",out_name);
printf(out_name); 

}