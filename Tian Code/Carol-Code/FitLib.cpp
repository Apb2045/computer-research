// FitLib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "FitLib.h"


#include "vjfacedetect.h"
//#include "video_camera.h"

asm_shape* pShape = NULL;
asm_shape* pDetshape = NULL;
bool bFlag = false;


FITLIB_API void* ASMFitCreate()
{
	asmfitting* pRet = new asmfitting;
	bFlag = false;
	pShape = new asm_shape;
	pDetshape = new asm_shape;
	return pRet;
}

FITLIB_API void ASMFitRelease(void *pASMFit)
{
	asmfitting* pWork = (asmfitting*)pASMFit;
	delete pShape;
	delete pDetshape;
	delete pWork;
}

FITLIB_API int ASMFitLoadModel(void *pASMFit, const char* pczModelFileName)
{
	asmfitting* pWork = (asmfitting*)pASMFit;
	if(pWork->Read(pczModelFileName))
		return 0;
	
	return -1;
}

FITLIB_API int ASMFitLoadCascade(const char* pczCascadeFileName)
{
	if(init_detect_cascade(pczCascadeFileName))
		return 0;

	return -1;
}

FITLIB_API int ASMFitProcessImage(void *pASMFit, unsigned char* pczImgData, int iImgWidth, 
								  int iImgHeight, int iImgWdthStep, const char* pczImgType, int iFrameNum, 
								  int iNumiteration, MLFaceAAMPoints* face_shape)
{
	// convert to input image to ipl image
	IplImage* pOutImg = cvCreateImageHeader(cvSize(iImgWidth, iImgHeight), IPL_DEPTH_8U, 3);
	pOutImg->imageData = (char*)pczImgData;
	pOutImg->origin = 1;

	int iH = iImgHeight / 2;
	for (int iY = 0; iY < iH; iY++)
	{
		unsigned char* pDst = pczImgData + (iImgHeight - iY) * iImgWdthStep;
		unsigned char* pSrc = pczImgData + iY * iImgWdthStep;
		for (int iX = 0; iX < iImgWdthStep; iX++)
		{
			unsigned char cVal = *pDst;
			*pDst++ = *pSrc;
			*pSrc++ = cVal;
		}
	}

	asmfitting* pFitAsm = (asmfitting*) pASMFit;
	// do face detection by using Viola_jones haarlike-detector
	
	if(iFrameNum == 0 || bFlag == false)
	{
		bFlag = detect_one_face(*pDetshape, pOutImg);
		
		// initialize shape from the detected box
		if(bFlag) 
			InitShapeFromDetBox(*pShape, *pDetshape, pFitAsm->GetMappingDetShape(), pFitAsm->GetMeanFaceWidth());
		else goto output;
	}
					
	//do ASM model fit 
	bFlag = pFitAsm->ASMSeqSearch(*pShape, pOutImg, iFrameNum, true, iNumiteration);

	//draw the shape model
	if(bFlag) 
	{
		pFitAsm->Draw(pOutImg, *pShape);

		//output the face model points
		int inum = pShape->NPoints();

		face_shape->m_iTotalNum = pShape->NPoints(); //total number of points 
		face_shape->m_iFrame = iFrameNum;//Frame number in the video

		//output detected corners
		CvPoint2D32f pt;
		CPoint pt1;

		for(int k = 0; k < pShape->NPoints(); k ++)
		{
			pt = (*pShape)[k];
			pt1.x = (int) pt.x;
			pt1.y = (int) (iImgHeight - pt.y);
			face_shape->m_points[k] = pt1;
		}

		face_shape->m_iCenterX = (int) (*pShape)[pShape->NPoints() - 1].x; //point 68 -- middle point of nose
		face_shape->m_iCenterY = (int) (*pShape)[pShape->NPoints() - 1].y; //point 68 -- middle point of nose
		face_shape->m_iMinX = (int) pShape->MinX(); // minimum x in all the points of face shape.
		face_shape->m_iMinY = (int) (iImgHeight - pShape->MaxY()); // minimum y in all the points of face shape.
		face_shape->m_iMaxX = (int) pShape->MaxX(); // maximum x in all the points of face shape.
		face_shape->m_iMaxY = (int) (iImgHeight - pShape->MinY()); // maximum y in all the points of face shape.
	}

output:
	pOutImg->imageData = NULL;
	cvReleaseImageHeader(&pOutImg);

	for (int iY = 0; iY < iH; iY++)
	{
		unsigned char* pDst = pczImgData + (iImgHeight - iY) * iImgWdthStep;
		unsigned char* pSrc = pczImgData + iY * iImgWdthStep;
		for (int iX = 0; iX < iImgWdthStep; iX++)
		{
			unsigned char cVal = *pDst;
			*pDst++ = *pSrc;
			*pSrc++ = cVal;
		}
	}

	return 0;
}