/*********************************************************************
 *File: InitializeFile.h
 *Purpose: include all header files in one place
 *
 *Author: Shizhi Chen
 *Email: schen21@ccny.cuny.edu
 *Date: 4/4/2012
 *********************************************************************/

#ifndef InitializeFile_H___
#define InitializeFile_H___

#ifdef _WIN32
const bool WINDOW_OS = true;
#include <mat.h>
#include <mex.h>
#else
const bool WINDOW_OS = false; //Mac OS
#endif

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <vector>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <cv.h> 
//#include <cxcore.h>
#include <highgui.h>
//#include <dirent.h>
#include "dirent.h"
#include <algorithm>
#include "boost/regex.hpp" //also need copy stage/lib/* to the folder which has executable file.

//#include <engine.h>

using namespace boost;
using namespace std;

//Laplacian of Gaussian filter sizes; Note: filter size of 9 corresponding to the scale of 1.2
//const int NUM_FILTERS = 1;
//const int filterSizes_LOG[NUM_FILTERS] = {9}; //9, 15, 21,27
//image boundary width, within which interest points are excluded
//const int BOUNDARY_WIDTH = 15;

const float PI = 3.14159f;

const float LARGE_POSNUMBER = 1000000000.0f; 
const float LARGE_NEGNUMBER = -1000000000.0f; 
const float SMALL_POSNUMBER = 1.2e-07f;

////non uniform bin for transform color (R-meanR)/sigmaR;(G-meanG)/sigmaG; (B-meanB)/sigmaB;
////use to approximate the intervals so that the gaussian probability has about same probabilities in each interval.
//const float intervals_gauss[7] = {-1000000000.0f,-1.0f,-0.4f,0.0f,0.4f,1.0f,1000000000.0f};













#endif
