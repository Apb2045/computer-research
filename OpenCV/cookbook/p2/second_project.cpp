#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

void salt(cv::Mat &image, int n);

int main(){
    cv::Mat image=cv::imread("water_castle.jpg");
    salt(image,300000);
    cv::namedWindow("Image");
    cv::imshow("Image", image);
    cv::waitKey(5000);
    
    return 0;}

void salt(cv::Mat &image, int n){
    for(int k=0;k<n;k++){
        int i=rand()%image.cols;
        int j=rand()%image.rows;

        if(image.channels()==1)
            image.at<uchar>(j,i)=255;
        
        else if(image.channels()==3){
              image.at<cv::Vec3b>(j,i)[0]=255;
              image.at<cv::Vec3b>(j,i)[1]=255;
              image.at<cv::Vec3b>(j,i)[2]=255;}
}
}
        

        

